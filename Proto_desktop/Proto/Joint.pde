/*
 * Join class that will draw a line between two objects to specify that they are 
 * connected to each other.
 * Author: Mahendra Rai
 */

class Joint{
  SoundObj A, B;
  
  //constructor
  public Joint(SoundObj A, SoundObj B){
    this.A = A;
    this.B = B;
  }
  
  //draws joint
  public void drawJoint(){
    line(A.getX(), A.getY(), B.getX(), B.getY());
    stroke(0);
    strokeWeight(2);
  }
}
