/*
 * SoundObj class extends from FCircle class provided by fisica library adding extra 
 * functionality of adding sound to the object and playing it.
 * Author: Mahendra Rai
 *
 */

import fisica.*;

class SoundObj extends FCircle{
  AudioPlayer player;
  float size;
  color[] colors = {#F5B502, #CAD401, #EA2D66, #35B7EE, #FFDF3B, #E71E03, #9BF451, #2B5996};
  
  //contructor
  public SoundObj(Maxim maxim, float size){
    super(size);
    
    //get random X and Y value for the object's position
    float posX = random(size/2, width-size/2);
    float posY = random(size/2, (height - 80)-size/2);
    
    //set object's attributes
    this.setPosition(posX, posY);
    this.setVelocity(random(-10,10), random(-10,10));
    this.setFillColor(colors[(int)random(0,8)]);
    this.setGrabbable(true);
    this.setStroke(0);
    this.setStrokeWeight(2);
    this.size = size;
    
    //instantiate player by loading appropriate sound file based on the object's colour
    this.player = maxim.loadFile(this.getSoundFilename(this.getFillColor()));
    this.player.volume(1.2);
  }
  
  //plays sound when the object is clicked
  public void playSound(float delay){
    if(this.player.isPlaying()){
      //cue the player to play the sound from beginning
      this.player.cue(0);
    }
    
    this.player.setLooping(false);
    this.player.speed(1);
    this.player.play();
  }
  
  //gets sound file based on the object's colour
  private String getSoundFilename(color objColor){
    String filename = null;
    
    switch(objColor){
      case #F5B502:
        filename = "bass1.wav";
        break;
      case #CAD401:
        filename = "bass2.wav";
        break;
      case #EA2D66:
        filename = "bass3.wav";
        break;
      case #35B7EE:
        filename = "bass4.wav";
        break;
      case #FFDF3B:
        filename = "bass5.wav";
        break;
      case #E71E03:
        filename = "bass6.wav";
        break;
      case #9BF451:
        filename = "bass7.wav";
        break;
      case #2B5996:
        filename = "bass8.wav";
        break;
    }
    
    return filename;
  }
}
