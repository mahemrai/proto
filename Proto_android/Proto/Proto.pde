/*
 * Project - Proto
 * Author - Mahendra Rai
 */

import fisica.*;

Maxim maxim;
AudioPlayer beats, ambience1, ambience2, ambience3;
FWorld world;
SoundObj object;

color actualObjColor;
boolean playBackground = false;
boolean playAmb1, playAmb2, playAmb3 = false;

int numOfObjects = 10;                  //number of objects at the start of the application
int objNumCounter = numOfObjects;       //counter for the object number so it doesn't exceed the total allowed number of objects
int totalNumOfObjects = 20;             //total number of allowed objects

//stores list of sound objects
ArrayList<SoundObj> objects = new ArrayList<SoundObj>();

Button addObject, playBeat, playAmbience1, playAmbience2, playAmbience3;

//initialise the app
void setup(){
  size(displayWidth, displayHeight);
  smooth();
  
  maxim = new Maxim(this);
  beats = maxim.loadFile("beat1.wav");
  beats.volume(0.5);
  
  ambience1 = maxim.loadFile("extra1.wav");
  ambience1.volume(0.5);
  
  ambience2 = maxim.loadFile("extra2.wav");
  ambience2.volume(0.5);
  
  ambience3 = maxim.loadFile("extra3.wav");
  ambience3.volume(0.5);
  
  //buttons
  addObject = new Button("Add Object", 20, displayHeight-60, 100, 40);
  playBeat = new Button("Play Beat", 140, displayHeight-60, 100, 40);
  playAmbience1 = new Button("Amb 1", 260, displayHeight-60, 100, 40);
  playAmbience2 = new Button("Amb 2", 320, displayHeight-60, 100, 40);
  playAmbience3 = new Button("Amb 3", 380, displayHeight-60, 100, 40);
  
  //fisica world initialisation
  Fisica.init(this);
  world = new FWorld();
  world.setEdges(0, 0, displayWidth, displayHeight-80, 255);
  world.setGravity(0, 0);
  world.setGrabbable(true);
  
  createObjects();
}

//draw world
void draw(){
  background(255);
  connectObjects();
  world.step();
  world.draw();
  drawGUI();
  
  if(playBackground){
    beats.play();
  }
  else beats.stop();
  
  if(playAmb1){
    ambience1.play();
  }
  else ambience1.stop();
  
  if(playAmb2){
    ambience2.play();
  }
  else ambience2.stop();
  
  if(playAmb3){
    ambience3.play();
  }
  else ambience3.stop();
}

//create sound objects and add it into the fisica world as well
//as the arraylist
void createObjects(){
  for(int i=0; i<numOfObjects; i++){
    float size = random(30, 80);
    SoundObj obj = new SoundObj(maxim, size);
    world.add(obj);
    objects.add(obj);
  }
}

//loops through each objects and connects them to each other by calculating the distance
void connectObjects(){
  SoundObj parentObj, childObj;
  Joint joint;
  
  for(int i=0; i<objects.size(); i++){
    for(int j=0; j<objects.size(); j++){
      //object that will be used to check other objects' distance to it
      parentObj = objects.get(i);
      //object that will be checked whether it is close enough to the parent object
      childObj = objects.get(j);
      
      //make sure that the selected object and the child object is not the same object
      if(parentObj != childObj){
        //calculate distance between parent and child objects
        float distance = dist(parentObj.getX(), parentObj.getY(), childObj.getX(), childObj.getY());
        //subtract object's size from the distance to avoid calculating distance from
        //the centre point of the objects
        distance = distance - (parentObj.size + childObj.size);
        
        //if the distance is less than 15 then create the joint
        if(distance < 15){
          joint = new Joint(parentObj, childObj);
          joint.drawJoint();
        }
      }
    }
  }
}

//check which objects are close to the object that has been clicked on in order to play 
//its sound to form a sequence of musical notes
void checkForClosestNeighbor(SoundObj parentObj){
  //loop through all the objects
  for(int i=0; i<objects.size(); i++){
    SoundObj childObj = objects.get(i);
      
    //make sure that the selected object and the child object is not the same object
    if(parentObj != childObj){
      //calculate distance between parent and child objects
      float distance = dist(parentObj.getX(), parentObj.getY(), childObj.getX(), childObj.getY());
      //subtract object's size from the distance to avoid calculating distance from
      //the centre point of the objects
      distance = distance - (parentObj.size + childObj.size);
      
      //if the distance is less than 15 then play the sound
      if(distance < 15){
        try{
          //delay the playing of the sound by 0.4ms
          Thread.sleep(400);
          childObj.playSound(0);
        }
        catch(Exception e){
        }
      }
    }
  }
}

//handle the mouse pressed event
void mousePressed(){
  world.grabBody(mouseX, mouseY);
  //grab the object if clicked on it
  object = (SoundObj)world.getBody(mouseX, mouseY);
  
  //check whether the mouse was pressed over an object
  if(object != null){
    //get the object's color
    actualObjColor = object.getFillColor();
    //play the sound
    object.playSound(0);
    //change color to notify that the object has been clicked on
    object.setFill(255);
  }
  else{
    //check if the mouse was pressed over an addObject button
    if(addObject.mousePressed()){
      //check if the current number of objects is less than the allowed number
      if(objNumCounter < totalNumOfObjects){
        //create new object and add it to the fisica world as well as the arraylist
        float size = random(30, 80);
        SoundObj obj = new SoundObj(maxim, size);
        world.add(obj);
        objects.add(obj);
        
        //update counter
        objNumCounter++;
      }
    }
    //check if the mouse was pressed over playBeat button
    else if(playBeat.mousePressed()){
      if(!playBackground){
        playBackground = true;
      }
      else playBackground = false;
    }
    //check if the mouse was pressed over playAmbience1 button
    else if(playAmbience1.mousePressed()){
      if(!playAmb1){
        playAmb1 = true;
      }
      else playAmb1 = false;
    }
    //check if the mouse was pressed over playAmbience2 button
    else if(playAmbience2.mousePressed()){
      if(!playAmb2){
        playAmb2 = true;
      }
      else playAmb2 = false;
    }
    //check if the mouse was pressed over playAmbience3 button
    else if(playAmbience3.mousePressed()){
      if(!playAmb3){
        playAmb3 = true;
      }
      else playAmb3 = false;
    }
  }
}

//handle the mouse release event
void mouseReleased(){
  world.releaseBody();
  //check if the mouse was pressed over an object before releasing
  if(object != null){
    //change its color to its original
    object.setFillColor(actualObjColor);
    checkForClosestNeighbor(object);
  }
  else{
    addObject.mouseReleased();
    playBeat.mouseReleased();
    playAmbience1.mouseReleased();
    playAmbience2.mouseReleased();
    playAmbience3.mouseReleased();
  }
}

//draws GUI
void drawGUI(){
  pushStyle();
  fill(51,51,51);
  noStroke();
  rect(0, displayHeight-80, displayWidth, 80);
  popStyle();
    
  addObject.display();
  playBeat.display();
  playAmbience1.display();
  playAmbience2.display();
  playAmbience3.display();
}

